use std::time::Duration;

use df_consul::*;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let config = Config {
        addr: "http://localhost:8500".into(),
        ca_cert: None,
        tls_skip_verify: false,
        client_cert: None,
        client_key: None,
    };

    let consul = Consul::new(config, "").unwrap();

    println!("== LIST NODES ==");
    let nodes = consul.catalog_node_list(None).await.unwrap();
    println!("{:?}", nodes);

    if let Some(node) = nodes.first() {
        println!("== NODE {} ==", node.node);
        println!("{:?}", consul.catalog_node(&node.node, None).await.unwrap());
    }

    println!("== LIST SERVICES ==");
    let services = consul.catalog_service_list(None).await.unwrap();
    println!("{:?}", services);

    if let Some(service) = services.keys().next() {
        println!("== SERVICE NODES {} ==", service);
        println!(
            "{:?}",
            consul.catalog_service_nodes(service, None).await.unwrap()
        );

        println!("== SERVICE HEALTH {} ==", service);
        println!(
            "{:?}",
            consul
                .health_service_instances(service, None)
                .await
                .unwrap()
        );
    }

    println!("== LIST PREFIX ==");
    let prefix = consul
        .kv_get_prefix("diplonat/autodiscovery", None)
        .await
        .unwrap();
    println!("{:?}", prefix);
    for i in 0..3 {
        println!("-- wait for update... --");
        let prefix = consul
            .kv_get_prefix("diplonat/autodiscovery", Some(prefix.index()))
            .await
            .unwrap();
        println!("{:?}", prefix);
    }
}
