//! Contains structures to interact with the locks/sessions API
//!
//! See <https://developer.hashicorp.com/consul/api-docs/session>
//! for the full definition of the API.

use anyhow::Result;
use bytes::Bytes;
use log::*;
use serde::{Deserialize, Serialize};

use crate::Consul;

/// Session creation request as specified in
/// <https://developer.hashicorp.com/consul/api-docs/session#create-session>
#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "PascalCase")]
pub struct SessionRequest {
    pub name: String,
    pub node: Option<String>,
    pub lock_delay: Option<String>,
    #[serde(rename = "TTL")]
    pub ttl: Option<String>,
    pub behavior: Option<String>,
}

/// (for internal use, mostly)
#[derive(Serialize, Deserialize, Debug)]
pub struct SessionResponse {
    #[serde(rename = "ID")]
    pub id: String,
}

impl Consul {
    pub async fn create_session(&self, req: &SessionRequest) -> Result<String> {
        debug!("create_session {:?}", req);

        let url = format!("{}/v1/session/create", self.url);
        let http = self.client.put(&url).json(req).send().await?;
        let resp: SessionResponse = http.json().await?;
        Ok(resp.id)
    }

    pub async fn acquire(&self, key: &str, bytes: Bytes, session: &str) -> Result<bool> {
        debug!("acquire {}", key);

        let url = format!(
            "{}/v1/kv/{}{}?acquire={}",
            self.url, self.kv_prefix, key, session
        );
        let http = self.client.put(&url).body(bytes).send().await?;
        let resp: bool = http.json().await?;
        Ok(resp)
    }

    pub async fn release(&self, key: &str, bytes: Bytes, session: &str) -> Result<()> {
        debug!("release {}", key);

        let url = format!(
            "{}/v1/kv/{}{}?release={}",
            self.url, self.kv_prefix, key, session
        );
        let http = self.client.put(&url).body(bytes).send().await?;
        http.error_for_status()?;
        Ok(())
    }
}
