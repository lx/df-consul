use std::fmt::{Debug, Display, Write};

use anyhow::{bail, Result};
use log::*;
use reqwest::Response;
use serde::Deserialize;

use crate::Consul;

impl Consul {
    pub(crate) async fn get_with_index<T: for<'de> Deserialize<'de>>(
        &self,
        mut url: String,
        last_index: Option<usize>,
    ) -> Result<WithIndex<T>> {
        if let Some(i) = last_index {
            if url.contains('?') {
                write!(&mut url, "&index={}", i).unwrap();
            } else {
                write!(&mut url, "?index={}", i).unwrap();
            }
        }
        debug!("GET {} as {}", url, std::any::type_name::<T>());

        let http = self.client.get(&url).send().await?;

        Ok(WithIndex::<T>::index_from(&http)?.value(http.json().await?))
    }
}

/// Wraps the returned value of an [API call with blocking
/// possibility](https://developer.hashicorp.com/consul/api-docs/features/blocking) with the
/// returned Consul index
pub struct WithIndex<T> {
    pub(crate) value: T,
    pub(crate) index: usize,
}

impl<T> WithIndex<T> {
    /// (for internal use, mostly)
    pub fn index_from(resp: &Response) -> Result<WithIndexBuilder<T>> {
        let index = match resp.headers().get("X-Consul-Index") {
            Some(v) => v.to_str()?.parse::<usize>()?,
            None => bail!("X-Consul-Index header not found"),
        };
        Ok(WithIndexBuilder {
            index,
            _phantom: Default::default(),
        })
    }

    /// Returns the inner value, discarding the index
    pub fn into_inner(self) -> T {
        self.value
    }

    /// Returns the Consul index, to be used in future calls to the same API endpoint to make them
    /// blocking
    pub fn index(&self) -> usize {
        self.index
    }
}

impl<T> std::convert::AsRef<T> for WithIndex<T> {
    fn as_ref(&self) -> &T {
        &self.value
    }
}

impl<T> std::borrow::Borrow<T> for WithIndex<T> {
    fn borrow(&self) -> &T {
        &self.value
    }
}

impl<T> std::ops::Deref for WithIndex<T> {
    type Target = T;
    fn deref(&self) -> &T {
        &self.value
    }
}

impl<T: Debug> Debug for WithIndex<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <T as Debug>::fmt(self, f)
    }
}

impl<T: Display> Display for WithIndex<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <T as Display>::fmt(self, f)
    }
}

/// (for internal use, mostly)
pub struct WithIndexBuilder<T> {
    _phantom: std::marker::PhantomData<T>,
    index: usize,
}

impl<T> WithIndexBuilder<T> {
    /// (for internal use, mostly)
    pub fn value(self, value: T) -> WithIndex<T> {
        WithIndex {
            value,
            index: self.index,
        }
    }
}
