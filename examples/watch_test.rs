use std::time::Duration;

use df_consul::*;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();

    let config = Config {
        addr: "http://localhost:8500".into(),
        ca_cert: None,
        tls_skip_verify: false,
        client_cert: None,
        client_key: None,
    };

    let consul = Consul::new(config, "").unwrap();

    println!("== WATCHING EVERYTHING ==");
    let mut watch = consul.watch_all_service_health(Duration::from_secs(30));
    loop {
        if watch.changed().await.is_err() {
            break;
        }
        //println!("\n{:?}", watch.borrow_and_update());
        println!("changed, {} values", watch.borrow_and_update().len());
    }
}
